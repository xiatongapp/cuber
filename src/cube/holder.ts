import * as THREE from "three";
export default class Holder {
    public vector: THREE.Vector3
    public index: number
    public plane: THREE.Plane
}